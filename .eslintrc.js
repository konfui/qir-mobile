module.exports = {
  'env': {
    'es6': true,
    'jest': true,
  },
  'extends': [
    'google',
    'plugin:react/recommended',
    'plugin:react-native/all'
  ],
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly'
  },
  'parser': 'babel-eslint',
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true,
    },
    'ecmaVersion': 2018,
    'sourceType': 'module',
  },
  'plugins': [
    'react',
    'react-native',
    'babel'
  ],
  'rules': {
    'require-jsdoc': 'off',
    'react-native/no-raw-text': 'off',
    'no-invalid-this': 0,
    'babel/no-invalid-this': 1
  },
  'settings': {
    'react': {
      'version': 'detect'
    }
  }
};
