// SPDX-License-Identifier: GPL-3.0-or-later
/* eslint-disable react/display-name */
'use strict';
import {Root} from 'native-base';
import React from 'react';
import {createAppContainer} from 'react-navigation';
import {BottomTabNavigator} from './src/navigation';
import './src/init';

const AppContainer = createAppContainer(BottomTabNavigator);

export default () =>
  <Root>
    <AppContainer />
  </Root>;
