#!/usr/bin/env node
const {Storage} = require('@google-cloud/storage');
const {basename} = require('path');

const uploadFile = async (bucketName, filePath) => {
  const storageClient = new Storage();
  const fileName = basename(filePath);

  await storageClient.bucket(bucketName).upload(filePath, {
    destination: 'releases/' + fileName,
    validation: 'md5',
    resumable: false,
  }, (err, _, apiResponse) => {
    if (err) {
      console.error('Error when storing to Google Cloud Storage', err);
      console.error('API response', apiResponse);
      return;
    }
  });
};

try {
  const bucketName = process.argv[2];
  const filePath = process.argv[3];
  uploadFile(bucketName, filePath);
  console.log('Uploaded', filePath, 'to', bucketName);
} catch (err) {
  console.error('Unable to store release build into Google Cloud Storage', err);
}
