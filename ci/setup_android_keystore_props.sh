#!/usr/bin/env bash
REVIEW_PROPS_TEMPLATE="
storePassword=TODO
keyPassword=TODO
keyAlias=qir-review
storeFile=~/qir.keystore
"

RELEASE_PROPS_TEMPLATE="
storePassword=TODO
keyPassword=TODO
keyAlias=qir-release
storeFile=~/qir.keystore
"

echo $REVIEW_PROPS_TEMPLATE > ../android/keystores/review.keystore.properties
echo $RELEASE_PROPS_TEMPLATE > ../android/keystores/release.keystore.properties
