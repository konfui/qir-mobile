---
image: node:8-alpine
variables:
  ANDROID_PROJECT: android
  REVIEW_KEYSTORE_PROPERTIES: review.keystore.properties
  RELEASE_KEYSTORE_PROPERTIES: release.keystore.properties
  GRADLE_TASK: assembleReview
  GCP_BUCKET_NAME: qir-ftui-cicd

.SetupKeystore: &before_script
  before_script:
    - echo $KEYSTORE_FILE_BASE64 | base64 -d > $ANDROID_PROJECT/app/qir.keystore
    - cat $REVIEW_PROPERTIES > $ANDROID_PROJECT/keystores/$REVIEW_KEYSTORE_PROPERTIES
    - cat $RELEASE_PROPERTIES > $ANDROID_PROJECT/keystores/$RELEASE_KEYSTORE_PROPERTIES

.BuildAPK: &apk_script
  script:
    - chmod +x $ANDROID_PROJECT/gradlew
    - pushd $ANDROID_PROJECT
    - ./gradlew --no-daemon
      -Dorg.gradle.jvmargs="-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap"
      $GRADLE_TASK
    - popd

stages:
  - build
  - test
  - package

include:
  - local: .gitlab/ci/global.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: License-Management.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml

sast:
  only:
    refs:
      - merge_requests
      - master
      - tags

code_quality:
  only:
    refs:
      - merge_requests
      - master
      - tags

dependency_scanning:
  extends: .only-deps-changes
  only:
    refs:
      - merge_requests
      - master
      - tags
  variables:
    DS_DEFAULT_ANALYZERS: "retire.js"

license_management:
  extends: .only-deps-changes
  only:
    refs:
      - merge_requests
      - master
      - tags

prepare_dependencies:
  extends:
  - .push-cache-job
  - .only-default
  stage: build
  script: yarn install --frozen-lockfile && yarn run jetify

lint_test:
  extends: 
    - .pull-cache-job
    - .only-default
  stage: test
  script: yarn lint
  allow_failure: true

unit_test:
  extends: 
    - .pull-cache-job
    - .only-default
  stage: test
  script: yarn test:with-coverage:on-ci
  coverage: '/All files[^|]*\|[^|]*\s+([\d\.]+)/'
  artifacts:
    reports:
      junit: junit.xml

build_review_variant:
  extends:
    - .pull-cache-job
    - .only-default
  image: reactnativecommunity/react-native-android:2019-6-25
  stage: package
  <<: *before_script
  variables:
    GRADLE_TASK: assembleReview
  <<: *apk_script
  artifacts:
    name: "$CI_COMMIT_REF_SLUG"
    paths:
      - $ANDROID_PROJECT/app/build/outputs/apk/review
    when: on_success
    expire_in: 1 week
  except:
    refs:
      - master

build_release_variant:
  extends:
    - .pull-cache-job
    - .only-default
  image: reactnativecommunity/react-native-android:2019-6-25
  stage: package
  <<: *before_script
  variables:
    GRADLE_TASK: assembleRelease
  <<: *apk_script
  after_script:
    - cat $GCP_SERVICE_ACCOUNT_KEY > credentials.json
    - export GOOGLE_APPLICATION_CREDENTIALS="$(pwd)/credentials.json"
    - mv $ANDROID_PROJECT/app/build/outputs/apk/release/*.apk .
    - bash ci/upload_apks_to_cloud_storage.sh
  except:
    refs:
      - branches
      - merge_requests
