'use strict';
import PropTypes from 'prop-types';
import React from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import NewsListItem from './NewsListItem';
import qir from '../../../native-base-theme/variables/qir';

// TODO Handle empty list of news items (e.g. display warning message?)
const NewsList = ({navigation, newsData}) =>
  <FlatList
    ItemSeparatorComponent={() => <View style={styles.separator} />}
    keyExtractor={(item, index) => String(index)}
    data={newsData}
    renderItem={(news) => <NewsListItem
      navigation={navigation}
      category={news.item.category}
      date={getDate(news.item.date)}
      headline={news.item.headline}
      link={news.item.link} />}
  />;

const getDate = (date) => {
  const dateSplit = date.split(' ');
  dateSplit.pop();
  return dateSplit.join(' ');
};

NewsList.propTypes = {
  navigation: PropTypes.object,
  newsData: PropTypes.array.isRequired,
};

const styles = StyleSheet.create({
  separator: {
    borderBottomColor: qir.brandLight,
    borderBottomWidth: 2,
  },
});

export default NewsList;
