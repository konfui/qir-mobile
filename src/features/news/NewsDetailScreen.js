'use strict';
import {H1, Spinner} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {WebView} from 'react-native-webview';
const NOT_FOUND = 'http://localhost';

const NewsDetailScreen = ({navigation}) =>
  <WebView source={{uri: navigation.getParam('uri', NOT_FOUND), method: 'GET'}}
    renderError={() => renderErrorMessage('Unable to load news page')}
    renderLoading={() => <Spinner />}
    startInLoadingState={true}
  />;

const renderErrorMessage = (text) => <H1>{text}</H1>;

NewsDetailScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default NewsDetailScreen;
