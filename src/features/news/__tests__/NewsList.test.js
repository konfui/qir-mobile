'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import NewsList from '../NewsList';

describe('A list of news headlines', () => {
  let listComponent = null;

  beforeEach(() => {
    const testDates = [
      new Date(Date.UTC(2018, 0, 17)),
      new Date(Date.UTC(2018, 0, 16)),
      new Date(Date.UTC(2018, 0, 15)),
      new Date(Date.UTC(2018, 0, 14)),
      new Date(Date.UTC(2018, 0, 13)),
    ];
    const testData = testDates.map((testDate, index) => {
      const news = {
        category: 'Test',
        date: testDate.toUTCString(),
        headline: `Headline #${index}`,
        link: 'http://localhost',
      };
      return news;
    });
    const mockNavigation = {
      push: jest.fn(),
    };

    listComponent = TestRenderer.create(
        <NewsList navigation={mockNavigation} newsData={testData} />
    );
  });

  it('matches its snapshot correctly', () => {
    expect(listComponent.toJSON()).toMatchSnapshot();
  });
});
