'use strict';
import {Spinner, Text} from 'native-base';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import {Status} from '../../../utils';
import NewsListContainer from '../NewsListContainer';

describe('A news list container component', () => {
  let mockNavigation = null;
  let component = null;

  beforeEach(() => {
    component = TestRenderer.create(
        <NewsListContainer navigation={mockNavigation} />
    );
    mockNavigation = {
      push: jest.fn(),
    };
  });

  describe('when offline', () => {
    beforeEach(() => {
      component.getInstance().setStatus(Status.OFFLINE);
    });

    it('matches its snapshot', () => {
      expect(component.toJSON()).toMatchSnapshot();
    });

    it('displays an error message', () => {
      const text = findComponentFromRootByType(component, Text);

      expect(text.props.children).toBe('You are offline!');
    });
  });

  describe('when error', () => {
    beforeEach(() => {
      component.getInstance().setStatus(Status.ERROR);
    });

    it('matches its snapshot', () => {
      expect(component.toJSON()).toMatchSnapshot();
    });

    it('displays an error message', () => {
      const text = findComponentFromRootByType(component, Text);

      expect(text.props.children).toBe('An error has occurred!');
    });
  });

  describe('when loading', () => {
    beforeEach(() => {
      component.getInstance().setStatus(Status.LOADING);
    });

    it('matches its snapshot', () => {
      expect(component.toJSON()).toMatchSnapshot();
    });

    it('displays a spinner component', () => {
      const spinner = findComponentFromRootByType(component, Spinner);

      expect(spinner).toBeDefined();
    });
  });

  describe('when success', () => {
    beforeEach(() => {
      component.getInstance().setStatus(Status.SUCCESS);
    });

    it('matches its snapshot', () => {
      expect(component.toJSON()).toMatchSnapshot();
    });
  });
});

const findComponentFromRootByType = (component, type) => {
  const rootInstance = component.root;

  return rootInstance.findByType(type);
};
