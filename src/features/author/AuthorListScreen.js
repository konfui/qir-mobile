'use strict';
import {Container, Content, Header} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {TextHeader2} from '../../components';
import AuthorListContainer from './AuthorListContainer';
import styles from './styles';

const AuthorListScreen = ({data, navigation}) =>
  <Container>
    <Header style={styles.header}>
      <TextHeader2 first="Authors" />
    </Header>
    <Content>
      <AuthorListContainer data={data} navigation={navigation} />
    </Content>
  </Container>;

AuthorListScreen.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  navigation: PropTypes.object.isRequired,
};

export default AuthorListScreen;
