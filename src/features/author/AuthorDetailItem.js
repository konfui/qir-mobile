// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';
import React from 'react';
import {Text, Icon} from 'native-base';
import {TouchableHighlight, View} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const AuthorDetailItem = ({title, date, time, location, onPress}) =>
  <View style={styles.container}>
    <Text style={styles.title}>{title}</Text>
    <View style={styles.horizontal}>
      <Icon style={styles.iconStyle}
        type="SimpleLineIcons" name="clock" />
      <Text style={styles.itemStyle}>   {date}, {time}</Text>
    </View>
    <View style={styles.horizontal}>
      <Icon style={styles.iconStyle}
        type="SimpleLineIcons" name="location-pin" />
      <Text style={styles.itemStyle}>   {location}   </Text>
    </View>
    <TouchableHighlight onPress={onPress}>
      <Text style={styles.viewDetail}>View Details</Text>
    </TouchableHighlight>
    <View style={styles.separator}/>
  </View>;

AuthorDetailItem.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default AuthorDetailItem;
