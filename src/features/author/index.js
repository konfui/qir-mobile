'use strict';
import AuthorDetailScreen from './AuthorDetailScreen';
import AuthorListScreen from './AuthorListScreen';

export {AuthorListScreen, AuthorDetailScreen};
