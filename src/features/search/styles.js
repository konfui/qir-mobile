'use strict';
import {StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

export default StyleSheet.create({
  separator: {
    borderWidth: 1,
  },
  space: {
    marginTop: 8,
  },
  itemStyle: {
    fontStyle: 'italic',
    color: qir.brandGrey,
    fontSize: 14,
  },
  name: {
    fontWeight: 'bold',
  },
  author: {
    fontSize: 14,
  },
  track: {
    fontSize: 14,
    color: qir.brandPrimary,
  },
  input: {
    fontSize: 14,
  },
  search: {
    alignSelf: 'center',
  },
  noMatch: {
    color: qir.textColor,
    margin: 8,
    fontSize: 14,
  },
});
