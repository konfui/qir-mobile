'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import SubmissionDetailScreen from '../SubmissionDetailScreen';

describe('A submission detail screen', () => {
  it('matches with the snapshot', () => {
    const fakeSubmission = {
      title: 'Lorem ipsum',
      date: '2019-05-04',
      time: '13:00 - 13:30',
      authors: 'Bruno Mars and Justin Bieber',
      abstract: 'A quick brown fox jumps over a lazy fox',
    };
    const mockNavigation = {
      getParam: jest.fn((param) => {
        return fakeSubmission;
      }),
    };

    const component = TestRenderer.create(
        <SubmissionDetailScreen navigation={mockNavigation} />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
