'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import SubmissionList from '../SubmissionList';

describe('A container component for submission list items', () => {
  const fakeData = [
    {
      id: '1',
      title: 'Test',
      authors: 'Giorno Giovanna',
      organization: 'Universitas Indonesia',
      date: '22-07-2019',
      time: '10:00 - 11:30',
    },
    {
      id: '3',
      title: 'Road Roller',
      authors: 'Dio Sama, Giorno Giovanna and Joko Sembung',
      date: '22-07-2019',
      time: '11:30 - 12:00',
    },
    {
      id: '45',
      title: 'Android-based Wireless Measurement Module',
      authors: 'Giorno Giovanna and Bruno Mars',
      organization: 'Universitas Indonesia',
      date: '23-07-2019',
      time: '11:30 - 12:00',
    },
  ];
  let mockNavigation = {};

  beforeEach(() => {
    mockNavigation = {push: jest.fn()};
  });

  it('matches with the snapshot', () => {
    const component = TestRenderer.create(
        <SubmissionList data={fakeData}
          navigation={mockNavigation} />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
