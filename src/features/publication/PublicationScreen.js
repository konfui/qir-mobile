'use strict';
import {Container, Content, Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import styles from './styles';
import {HeaderBurger} from '../../components';

const PublicationScreen = ({navigation}) =>
  <Container>
    <HeaderBurger title='Publication' navigation={navigation}/>
    <Content>
      <Text style={styles.itemStyle}>
        All accepted and presented papers will be published
          in Scopus indexed publisher:{'\n'}{'\n'}
        {'\u2022'} AIP conference proceeding{'\n'}{'\n'}
        Selected accepted papers will have opportunity
          to be published in:{'\n'}{'\n'}
        {'\u2022'} The International Journal of Technology (IJTech)
             (ISSN: 20869614), indexed in SCOPUS (SJR Q2).{'\n'}
        {'\u2022'}The Material Science Forum Journal (ISSN: 02555476),
             indexed in SCOPUS (SJR Q3).{'\n'}
        {'\u2022'}The Evergreen Journal (ISSN: 21890420),
             indexed in SCOPUS (SJR Q3).{'\n'}
        {'\u2022'}The Makara Journal of Technology Series (ISSN: 2355-2786),
             indexed in Web of Science (ESCI){'\n'}{'\n'}
        Accepted and presented papers will be submitted for
          inclusion into IEEE Xplore as well as other
          Abstract and Indexing (A&I) database
      </Text>
    </Content>
  </Container>;

PublicationScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default PublicationScreen;
