'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import PublicationScreen from '../PublicationScreen';

describe('A publication screen component', () => {
  it('matches with the snapshot', () => {
    const mockNavigation = {push: jest.fn()};
    const component = TestRenderer.create(
        <PublicationScreen navigation={mockNavigation}/>
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
