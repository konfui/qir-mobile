'use strict';
import React from 'react';
import SplashScreen from 'react-native-splash-screen';
import DemoContainer from './DemoContainer';
import MessageListContainer from '../message';

class MessageDemo extends React.Component {
  componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    return (
      <DemoContainer>
        <MessageListContainer />
      </DemoContainer>
    );
  }
}

export default MessageDemo;
