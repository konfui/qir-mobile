import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/storage';

const config = {
  apiKey: 'AIzaSyBOX1uI3p6nN0XOTxR-P0xek-jajQj1Tng',
  projectId: 'qir-ftui',
  storageBucket: 'qir-ftui.appspot.com',
};

const app = firebase.initializeApp(config);
const storage = app.storage();
export const mapStorageRef = storage.ref('/venueMap/');
export const sponsorStorageRef = storage.ref('/sponsor/');
export const firestore = app.firestore();
