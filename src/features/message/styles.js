'use strict';
import {StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: 'column',
  },
  messageListContainer: {
    flex: 1,
    display: 'flex',
  },
  inputContainer: {
    flexDirection: 'row',
    borderTopColor: qir.brandLight,
    borderTopWidth: 1,
    paddingHorizontal: 4,
  },
  textInput: {
    flex: 8,
    fontSize: 14,
    color: qir.textColor,
    backgroundColor: qir.brandLighter,
    margin: 4,
    borderRadius: 8,
  },
  submitButton: {
    fontSize: 22,
    color: qir.brandPrimary,
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: 8,
    padding: 4,
  },
  adminBubble: {
    backgroundColor: qir.brandLight,
  },
  // eslint-disable-next-line react-native/no-color-literals
  userBubble: {
    backgroundColor: '#b3a4d5',
  },
  bubble: {
    borderRadius: 10,
    paddingBottom: 8,
    paddingHorizontal: 14,
    paddingTop: 8,
    minWidth: 0,
    maxWidth: '70%',
  },
  adminBubbleContainer: {
    flexDirection: 'row',
  },
  userBubbleContainer: {
    flexDirection: 'row-reverse',
  },
  bubbleContainer: {
    width: '100%',
    display: 'flex',
    marginBottom: 6,
    marginTop: 6,
    paddingLeft: 12,
    paddingRight: 12,
  },
  bubbleContent: {
    fontSize: 14,
  },
  timestamp: {
    alignSelf: 'flex-end',
    marginLeft: 8,
    marginRight: 8,
    color: '#aaaaaa',
  },
  noChats: {
    textAlign: 'center',
    paddingTop: 32,
    fontSize: 20,
  },
  addPadding: {
    height: 60,
  },
});
