'use strict';
import MessageListContainer from './MessageListContainer';
import MessageScreen from './MessageScreen';

export default {MessageListContainer, MessageScreen};
