'use strict';
import React from 'react';
import {View} from 'react-native';
import {Spinner} from 'native-base';
import MessageList from './MessageList';
import MessageInput from './MessageInput';
import {firestore} from '../../firebase';
import firebase from 'firebase';
import DeviceInfo from 'react-native-device-info';
import styles from './styles';

class MessageContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      chats: [],
      loading: true,
      chatroom: DeviceInfo.getUniqueID(),
    };
    this.populateChats = this.populateChats.bind(this);
    this.inputSubmit = this.inputSubmit.bind(this);
  }

  componentDidMount() {
    this.collectionRef = firestore.collection('chatroom')
        .doc(this.state.chatroom).collection('chats');
    this.initChatroom().then(() => {
      this.populateChats().then(() => {
        this.setState({loading: false});
      });
    });
  }

  initChatroom = async () => {
    return new Promise((resolve) => {
      firestore.collection('chatroom')
          .doc(this.state.chatroom).get().then(function(doc) {
            if (!doc.exists) {
              firestore.collection('chatroom').doc(this.state.chatroom).set({
                created_at: firebase.firestore.FieldValue.serverTimestamp(),
              });
            }
            resolve();
          }.bind(this));
    });
  }

  populateChats = async () => {
    return new Promise((resolve) => {
      this.collectionRef.orderBy('timestamp').onSnapshot((snapshot) => {
        if (this.state.chats.length === 0) {
          const chats = [];
          if (snapshot.docs.length !== 0) {
            snapshot.forEach(function(doc) {
              chats.push(
                  {id: doc.id, data: doc.data({serverTimestamps: 'estimate'})});
            });
          }
          this.setState({chats: chats});
        } else {
          let incomingChat = {};
          snapshot.docChanges().forEach(function(change) {
            if (change.type === 'added') {
              incomingChat = {id: change.doc.id,
                data: change.doc.data({serverTimestamps: 'estimate'})};
              this.setState({chats: [...this.state.chats, incomingChat]});
            }
          }.bind(this));
        }
        resolve();
      });
    });
  }


  inputSubmit(chat) {
    this.collectionRef.add({
      content: chat,
      from_admin: false,
      read: false,
      timestamp: firebase.firestore.FieldValue.serverTimestamp(),
    });
  }


  render() {
    if (this.state.loading) {
      return (
        <Spinner />
      );
    } else {
      return (
        <View style={styles.wrapper}>
          <MessageList chats={this.state.chats}/>
          <MessageInput submitAction={this.inputSubmit} />
        </View>
      );
    }
  }
}

export default MessageContainer;
