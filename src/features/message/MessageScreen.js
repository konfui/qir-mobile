'use strict';
import React from 'react';
import SplashScreen from 'react-native-splash-screen';
import MessageContainer from './MessageContainer';

class MessageScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    return (
      <MessageContainer/>
    );
  }
}

export default MessageScreen;
