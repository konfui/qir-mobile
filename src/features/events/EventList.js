'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import {FlatList} from 'react-native';
import EventListItem from './EventListItem';
import DetailListItem from './DetailListItem';

const EventList = ({eventData, navigation, keynoteData}) =>
  <FlatList
    keyExtractor={(item, index) => `${item.id}${index}`}
    data={eventData}
    renderItem={(event) => createListItem(event, navigation, keynoteData)} />;

const createListItem = ({item}, navigation, keynotes) => {
  if (item.isParallel === 'TRUE') {
    return (<EventListItem
      title={item.program}
      date={item.date}
      time={item.time}
      place={item.place}
      speaker={item.speaker}
      navigation={navigation}
      isParallel
    />);
  } else if (item.isChild === 'TRUE') {
    console.log('EventList#createListItem()', 'Child item', item);
    return <DetailListItem
      title={item.program}
      place={item.place}
    />;
  } else {
    return (<EventListItem
      title={item.program}
      date={item.date}
      time={item.time}
      place={item.place}
      speaker={item.speaker}
      navigation={navigation}
      keynote={keynotes.filter((entry) => entry.name === item.speaker)[0]}
    />);
  }
};

EventList.propTypes = {
  keynoteData: PropTypes.arrayOf(PropTypes.object).isRequired,
  eventData: PropTypes.arrayOf(PropTypes.object).isRequired,
  navigation: PropTypes.object,
};

export default EventList;
