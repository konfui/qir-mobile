// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import createEventTabHeading from '../createEventTabHeading';

describe('An event tab heading component', () => {
  it('matches with the snapshot', () => {
    const EventTabHeading = createEventTabHeading(
        new Date(Date.UTC(2019, 1, 2)),
        'Gang Star Party');
    const component = TestRenderer.create(EventTabHeading);

    expect(component.toJSON()).toMatchSnapshot();
  });
});
