'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import MainMenuScreen from '../MainMenuScreen';

describe('A main menu screen', () => {
  let mockNavigation = {};

  beforeEach(() => {
    mockNavigation = {
      push: jest.fn(),
    };
  });

  it('matches with the snapshot', () => {
    const component = TestRenderer.create(
        <MainMenuScreen navigation={mockNavigation} />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
