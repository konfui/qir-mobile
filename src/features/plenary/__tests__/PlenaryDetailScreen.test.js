// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import PlenaryDetailScreen from '../PlenaryDetailScreen';

describe('A plenary detail screen component', () => {
  const sampleData = {
    name: 'Giorno Giovanna',
    affiliation: 'Gang Star',
    country: 'Italy',
    profile: 'I, Giorno Giovanna, have a dream.',
    storagePath: 'speakers/test/giorno_giovanna.jpg',
  };
  let mockNavigation = {};

  beforeEach(() => {
    mockNavigation = {getParam: jest.fn(() => sampleData)};
  });

  it('matches with the snapshot', () => {
    const component = TestRenderer.create(
        <PlenaryDetailScreen navigation={mockNavigation} />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
