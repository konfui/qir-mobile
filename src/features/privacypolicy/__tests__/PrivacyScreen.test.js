'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import PrivacyScreen from '../PrivacyScreen';

describe('A privacy screen component', () => {
  it('matches with the snapshot', () => {
    const mockNavigation = {push: jest.fn()};
    const component = TestRenderer.create(
        <PrivacyScreen navigation={mockNavigation}/>
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
