'use strict';
import {StyleSheet} from 'react-native';
import qir from '../../../native-base-theme/variables/qir';

export default StyleSheet.create({
  header: {
    backgroundColor: qir.brandPrimary,
  },
  listItemContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 8,
  },
  listItemIcon: {
    fontSize: 36,
  },
  listItemLeftColumn: {
    flex: 3,
    flexDirection: 'column',
  },
  listItemRightColumn: {
    alignItems: 'flex-end',
    flex: 1,
    marginEnd: 16,
  },
  listItemTimePlaceText: {
    fontSize: 14,
    marginTop: 2,
  },
  listItemTitleText: {
    fontWeight: 'bold',
  },
  container: {
    backgroundColor: qir.brandPrimary,
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 8,
  },
  // eslint-disable-next-line react-native/no-color-literals
  title: {
    color: 'white',
    fontWeight: 'bold',
    marginHorizontal: 2,
    textTransform: 'uppercase',
  },
  textMargin: {
    margin: 20,
  },
  textBold: {
    fontWeight: 'bold',
  },
});

