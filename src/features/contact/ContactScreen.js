'use strict';
import {Container, Content, Text} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import styles from './styles';
import {HeaderBurger} from '../../components';

const ContactScreen = ({navigation}) =>
  <Container>
    <HeaderBurger title='Organizer' navigation={navigation}/>
    <Content>
      <Text style={styles.textMargin}>
            Organizing Committee Secretariat{'\n'}
            Dean’s Building, 2nd Floor{'\n'}
            Faculty of Engineering,{'\n'}
            Universitas Indonesia 16424{'\n'}
            Depok, West Java Indonesia{'\n'}
            Phone : +62-21-7863504{'\n'}
            e-mail :
        <Text style={styles.textBold}>
              qir@eng.ui.ac.id
        </Text>
      </Text>
    </Content>
  </Container>;

ContactScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default ContactScreen;
