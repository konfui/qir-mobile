'use strict';
import PropTypes from 'prop-types';
import React from 'react';
import {FlatList} from 'react-native';
import {
  Container,
  Content,
  Body,
  Card,
  CardItem,
  Left,
  Text,
} from 'native-base';
import LegalNotice from '../../assets/legal.json';
import {HeaderBurger} from '../../components';
import styles from './styles';

const LegalScreen = ({navigation}) =>
  <Container>
    <HeaderBurger title='Legal Notice' navigation={navigation}/>
    <Content>
      <FlatList
        data={LegalNotice}
        keyExtractor={(item) => item.title}
        renderItem={({item}) => (
          <Card>
            <CardItem header>
              <Left>
                <Text style={styles.title}>{item.title}</Text>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <Text style={styles.itemStyle}>{item.content}</Text>
              </Body>
            </CardItem>
          </Card>
        )} />
    </Content>
  </Container>;

LegalScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default LegalScreen;
