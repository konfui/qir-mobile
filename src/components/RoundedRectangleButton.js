'use strict';
import React from 'react';
import {StyleSheet, Text, TouchableHighlight, View} from 'react-native';
import PropTypes from 'prop-types';
import qir from '../../native-base-theme/variables/qir';

const RoundedRectangleButton = ({route, children, text, navigation}) =>
  <TouchableHighlight underlayColor={qir.brandLighter}
    onPress={navigation ? () => navigation.push(route) :
    () => console.log(`Button clicked! Route: ${route}`)}>
    <View style={styles.container}>
      <View style={styles.iconContainer}>
        {children}
      </View>
      <View style={styles.labelContainer}>
        <Text style={styles.label}>{text.replace('<br/>', '\n')}</Text>
      </View>
    </View>
  </TouchableHighlight>;

RoundedRectangleButton.propTypes = {
  route: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
  text: PropTypes.string.isRequired,
  navigation: PropTypes.object,
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  iconContainer: {
    alignItems: 'center',
    backgroundColor: qir.brandSoft,
    borderRadius: 8,
    borderWidth: 2,
    flex: 2,
  },
  label: {
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  labelContainer: {
    flex: 1,
    flexWrap: 'wrap',
    marginTop: 8,
    paddingHorizontal: 8,
  },
});

export default RoundedRectangleButton;
