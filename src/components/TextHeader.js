'use strict';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {H1} from 'native-base';
import PropTypes from 'prop-types';
import qir from '../../native-base-theme/variables/qir';

const TextHeader = ({backgroundColor, children, textColor}) =>
  <View style={{backgroundColor: backgroundColor}}>
    <H1 style={[styles.text, {color: textColor}]}>
      {children}
    </H1>
  </View>;

TextHeader.propTypes = {
  backgroundColor: PropTypes.string,
  children: PropTypes.string.isRequired,
  textColor: PropTypes.string,
};

TextHeader.defaultProps = {
  backgroundColor: qir.brandPrimary,
  children: 'Text Header',
  textColor: '#fffefe',
};

const styles = StyleSheet.create({
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    padding: 8,
    textTransform: 'uppercase',
  },
});

export default TextHeader;
