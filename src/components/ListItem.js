'use strict';
import {Icon} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {TouchableHighlight, View} from 'react-native';
import styles from './styles';
import qir from '../../native-base-theme/variables/qir';

const ListItem = ({children, onPress}) =>
  <View style={styles.container}>
    <TouchableHighlight underlayColor={qir.brandLight}
      style={styles.leftColumn} onPress={onPress}>
      <View name='clickable'>
        {children}
      </View>
    </TouchableHighlight>
    <TouchableHighlight underlayColor={qir.brandLight}
      style={styles.rightColumn} onPress={onPress}>
      <Icon style={styles.iconStyle} type="SimpleLineIcons"
        name='arrow-right' />
    </TouchableHighlight>
  </View>;

ListItem.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  onPress: PropTypes.func.isRequired,
};

export default ListItem;
