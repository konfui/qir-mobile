'use strict';
import PropTypes from 'prop-types';
import React from 'react';
import {Dimensions, Image, StyleSheet} from 'react-native';

const MapImage = ({source}) =>
  <Image
    resizeMode='contain'
    style={isPortrait() ? styles.portraitImage : styles.image}
    source={source} />;

const win = Dimensions.get('window');
const ratio = win.width/541;
const isPortrait = () => {
  const {width, height} = Dimensions.get('window');
  return height > width;
};

const styles = StyleSheet.create({
  image: {
    // flex: 1,
    // alignSelf: 'stretch',
    height: 362*ratio,
    width: win.width,
  },
  portraitImage: {
    // flex: 1,
    alignSelf: 'flex-start',
    padding: 1,
    // height: 362*ratio,
    width: win.width,
  },
});

MapImage.propTypes = {
  source: PropTypes.oneOfType([
    PropTypes.object, // Data type when running unit testing
    PropTypes.number, // Data type when running on device
  ]).isRequired,
};

export default MapImage;
