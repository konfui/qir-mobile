'use strict';
import {H1} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {View} from 'react-native';

const PlaceholderContainer = ({text}) =>
  <View>
    <H1>{text}</H1>
  </View>;

PlaceholderContainer.propTypes = {
  text: PropTypes.string.isRequired,
};

PlaceholderContainer.defaultProps = {
  text: 'Placeholder',
};

export default PlaceholderContainer;
