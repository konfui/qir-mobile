'use strict';
import {H1} from 'native-base';
import PropTypes from 'prop-types';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import qir from '../../native-base-theme/variables/qir';

const TextHeader2 = ({first, second}) =>
  <View style={styles.container}>
    <H1 style={styles.first}>{first}</H1>
    <H1 style={styles.second}>{second}</H1>
  </View>;

const styles = StyleSheet.create({
  container: {
    backgroundColor: qir.brandPrimary,
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 8,
  },
  // eslint-disable-next-line react-native/no-color-literals
  first: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    marginHorizontal: 2,
    textTransform: 'uppercase',
  },
  // eslint-disable-next-line react-native/no-color-literals
  second: {
    color: 'white',
    fontSize: 16,
    marginHorizontal: 2,
    textTransform: 'uppercase',
  },
});

TextHeader2.propTypes = {
  first: PropTypes.string.isRequired,
  second: PropTypes.string,
};

export default TextHeader2;
