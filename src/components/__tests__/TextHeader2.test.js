'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';
import TextHeader2 from '../TextHeader2';

describe('A text header with two-words', () => {
  it('matches its snapshot correctly', () => {
    const component = TestRenderer.create(
        <TextHeader2 first="Lorem" second="Ipsum" />
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
