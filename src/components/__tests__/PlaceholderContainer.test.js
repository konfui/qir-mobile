'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import PlaceholderContainer from '../PlaceholderContainer';

describe('a placeholder component', () => {
  it('matches snapshot correctly', () => {
    const placeholderComponent = renderer.create(
        <PlaceholderContainer text="Lorem Ipsum" />
    );

    expect(placeholderComponent.toJSON()).toMatchSnapshot();
  });
});
