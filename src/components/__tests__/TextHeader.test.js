'use strict';
import React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import TextHeader from '../TextHeader';

describe('A text header component', () => {
  it('matches its snapshot correctly', () => {
    const textHeaderComponent = renderer.create(
        <TextHeader>Test</TextHeader>
    );

    expect(textHeaderComponent.toJSON()).toMatchSnapshot();
  });
});
