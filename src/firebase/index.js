// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';

const CONFIG = {
  apiKey: 'AIzaSyBOX1uI3p6nN0XOTxR-P0xek-jajQj1Tng',
  projectId: 'qir-ftui',
  storageBucket: 'qir-ftui.appspot.com',
};

const app = firebase.initializeApp(CONFIG);
const firestore = app.firestore();
const storage = app.storage();

export {firestore, storage};
