// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';

const assets = {
  header_logo: require('./header_logo_v2.jpg'),
  image_placeholder: require('./image_placeholder.png'),
  profile_placeholder: require('./profile_placeholder.png'),
};

export default assets;
