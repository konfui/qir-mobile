// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';

export default (date) => {
  let day = 'Sunday';
  switch (date.getDay()) { // 0 - 6: 0 = Sunday, 6 = Saturday
    case 0: day = 'Sunday'; break;
    case 1: day = 'Monday'; break;
    case 2: day = 'Tuesday'; break;
    case 3: day = 'Wednesday'; break;
    case 4: day = 'Thursday'; break;
    case 5: day = 'Friday'; break;
    case 6: day = 'Saturday'; break;
  }

  return day.slice(0, 3);
};
