// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';
import createPhotoFilename from './createPhotoFilename';
import getDayShorthand from './getDayShorthand';

const getData = async (url, query) => {
  const options = {
    method: 'GET',
    cache: 'default',
    headers: {
      'Content-Type': 'application/vnd.api+json',
    },
  };
  let queryString = '';

  if (query !== undefined) {
    Object.keys(query).forEach((key) => {
      queryString += `${key}=${query[key]}&`;
    });
  }

  const requestUrl = `${url}?${queryString}`;

  try {
    const response = await fetch(requestUrl, options);
    return await response.json();
  } catch (error) {
    throw new Error(`Unable to fetch data from ${requestUrl}`);
  }
};

const Status = {
  ERROR: -2,
  OFFLINE: -1,
  SUCCESS: 0,
  ONLINE: 0,
  LOADING: 1,
};

const LoadStatus = {
  ERROR: -3,
  EMPTY: -2,
  LOADING: -1,
  LOADED: 0,
};

const ConnectionStatus = {
  OFFLINE: -1,
  ONLINE: 0,
};

export {ConnectionStatus, createPhotoFilename, getData, getDayShorthand,
  LoadStatus, Status};
