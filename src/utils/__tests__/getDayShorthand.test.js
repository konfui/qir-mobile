// SPDX-License-Identifier: GPL-3.0-or-later
'use strict';
import getDayShorthand from '../getDayShorthand';

describe('getDayShorthand()', () => {
  const fixtures = {
    0: [new Date(Date.UTC(2019, 0, 6)), 'Sun'],
    1: [new Date(Date.UTC(2019, 0, 7)), 'Mon'],
    2: [new Date(Date.UTC(2019, 0, 8)), 'Tue'],
    3: [new Date(Date.UTC(2019, 0, 9)), 'Wed'],
    4: [new Date(Date.UTC(2019, 0, 10)), 'Thu'],
    5: [new Date(Date.UTC(2019, 0, 11)), 'Fri'],
    6: [new Date(Date.UTC(2019, 0, 12)), 'Sat'],
  };

  Object.keys(fixtures).forEach((i) => {
    it(`parses ${fixtures[i]} correctly`, () => {
      expect(getDayShorthand(fixtures[i][0])).toEqual(fixtures[i][1]);
    });
  });
});
